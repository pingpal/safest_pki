<?php

namespace Safest\PKI;

use Safest\PKI\Config;

class CA {

	function signCSR($csr){


		$cacert = file_get_contents(realpath(Config::$CA_CERT_PATH));
		$pemcakey = file_get_contents(realpath(Config::$CA_KEY_PATH));
		
		$cnf = ['config' => realpath(Config::$OPENSSL_CONFIG_PATH)];

		$cnf['digest_alg'] = 'sha256';
		$cnf['x509_extensions'] = 'usr_cert';

		$privkeyobj = new \Crypt_RSA();
		$privkeyobj->setPassword(Config::$CA_KEY_PASSPHRASE);
		$privkeyobj->loadKey($pemcakey);



		$subject = new \File_X509();
		$subject->loadCSR($csr);
		//var_dump($privkeyobj);

		$issuer = new \File_X509();
		$issuer->loadX509($cacert);
		$issuer->setPrivateKey($privkeyobj);


		$x509 = new \File_X509();
		$x509->setSerialNumber(pack('N', time()));
		$x509->setStartDate('-1 day'); 
		$x509->setEndDate('+' . intval(Config::$CERT_VAL_PERIOD) . ' days');

		$result = $x509->sign($issuer, $subject);
		$res = $x509->saveX509($result);

		//var_dump($res);
		return $res;
	}

	function extractUidCSR($csr){

		$array = openssl_csr_get_subject($csr);
		return ($array && count($array) > 0) ? $array["CN"] : "";
	}

	function extractUidCert($cert){

		$array = openssl_x509_parse($cert);
		return ($array && count($array) > 0) ? $array["subject"]["CN"] : "";
	}

	function verifySignature($cert, $signature, $msg){
		$pubkey = openssl_pkey_get_public($cert);
		
		$ok = openssl_verify(base64_decode($msg), base64_decode($signature), $pubkey,"sha256WithRSAEncryption");
		$ret = false;
		if ($ok == 1) {
		    $ret = true;
		}
		
		openssl_free_key($pubkey);
		return $ret;
	}

	function getIssuer(){
		$cacertpath = 'file://'.realpath(Config::$CA_CERT_PATH);
		openssl_x509_export($cacertpath, $cacert);
		return $cacert;
	}
}
