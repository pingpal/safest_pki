<?php

namespace Safest\PKI\PKITests;

// use PPSecServer\Database;

use Safest\PKI\Database;
use Safest\PKI\PwdRevocation;
use Safest\PKI\Config;
use Safest\PKI\CA;

class DatabaseTest extends \PHPUnit_FrameWork_TestCase {
	
	private $uid;
	private $cert;
	private $pubCryptKey;

	protected function setUp()
    {

    	\Illusion\Core\Boot::strap();

    	$this->uid = substr(md5(rand()), 0, 32) . "s";
    	$this->uid2 = substr(md5(rand()), 0, 32) . "s";
    	$this->uidbad1 = substr(md5(rand()), 0, 33) . substr(md5(rand()), 0, 33);
    	$this->uidbad2 = 12345678901234567890123456789012;
		$this->cert = "-----BEGIN CERTIFICATE-----\nMIIEKDCCAxCgAwIBAgIBADANBgkqhkiG9w0BAQQFADCBjDELMAkGA1UEBhMCU0Ux\nDzANBgNVBAgTBlNjYW5pYTENMAsGA1UEBxMETHVuZDETMBEGA1UEChMKUGluZ1Bh\nbCBBQjEMMAoGA1UECxMDRGV2MRgwFgYDVQQDEw9QaW5nUGFsIG1haW4gQ0ExIDAe\nBgkqhkiG9w0BCQEWEWpvYWtpbUBwaW5ncGFsLnNlMB4XDTE0MDYwMjA5MDE1M1oX\nDTE1MDYwMjA5MDE1M1owKzEpMCcGA1UEAwwgZXlvcWN3d2ppa2dyZmJ3Ym9yY3Zw\nanJmaGFtZGVreHQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDcq82Q\n3eqQF+TQQWSOuCeDX0C9YXsk02riGTGbPIyDVZ3Pb8ZAI5ZnZROm+nj6ZDynkW4a\nYLOtEI6Q9jeudNgGh9fdBWrTEQSReO+1TTWJfEUJX/Pwg52QE+4dvrZCwIi17Dlb\npcNW/fkL8dbP5FUMIQI2OGQlED5/e3g8j7ycAjjaTgKt5DHiJcw5G7BoucZ395LL\nhml0TqBuG30TKEvia+/XS/qClFgGqOivEpQnn5dsTkOU6p29QlVxwwj/TIwneqoa\nZNnb5XJeEmN0QVVtA+wZrIVAkQtBiFeEUSBv2gKYBuk9hFf6Xdd/INVyD7hBcR55\nsRMjxr9IbduLJSkrAgMBAAGjgfQwgfEwHQYDVR0OBBYEFKvVR3sk0D1OueodZ+tJ\ncXdp3GhMMIHBBgNVHSMEgbkwgbaAFDPBZQ7aqlkCLk29KiB0R+oVnwzPoYGSpIGP\nMIGMMQswCQYDVQQGEwJTRTEPMA0GA1UECBMGU2NhbmlhMQ0wCwYDVQQHEwRMdW5k\nMRMwEQYDVQQKEwpQaW5nUGFsIEFCMQwwCgYDVQQLEwNEZXYxGDAWBgNVBAMTD1Bp\nbmdQYWwgbWFpbiBDQTEgMB4GCSqGSIb3DQEJARYRam9ha2ltQHBpbmdwYWwuc2WC\nCQCemMuJ2IeabDAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBAUAA4IBAQCQTjj3\ne6BG0Hpk77yeT0WwoBgIn0tBeNdWeRWetZ+BiR55i4OUSk3BTF5tH1peV5iMYF6w\nK+AUhRqPROoITOy4wTkPC6ApSqG6zGcx17de/HRl7Px3+4V5ukGWEo8pA5iNOu8B\nPAgO+OAWUQX6uAf4FsqYtdbshJErx9tg9hFoiOoUlI9g66hRRiunnpgm8zw9LCeU\nxYL0jgA+hCTgXJtRx8+0Ln+zE1nH9xap1LXe/veEx5VA+w4oR30zqHOoj0Us+wfu\ncmPxg56ehEg7vREXFjLmov9O0frGjgj/giEH/HiNK+Q9ma19+45owqTFdWigABnB\n3yyG2Mdsnoda+jkI\n-----END CERTIFICATE-----\n";
		$this->pCryptKey = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 44);
		$this->pCryptKey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 44);
		$this->signedPCryptKey = "NJvFVG5SXHR9PRejNskP3Cp3T4u8dL4YuLPfUyJO50H0tRS7zcTCHftDcC1m6vKU\nWhgB1tVB1aRAh0eDWGhyzdxR5A9ymmlsrlw7hhOV8Os/1cIWALAzrXd+NYV4utP9\nJpXLY2z2KACKfYK5UR6Jg/SSbRVp2lbBgR4RW3CJqQeCXoYhw+S9kwhXGCrrYdd0\ngUUFWpAAbZ9sF85PqUnv+X/+IdyyZ5NQTf1S0u5imWVGLlzTgibYVpC88Az8rK1U\nDlsXfx/F2DBq0azRtZLY2dcWOcjQSeD5KDK0CpdFWk/XuvBfVGxKiuYOYDqGKuRx\noM4E0SXTRJn2Jx2F3y1/Dg==";
    	$ca = new CA();
    	$this->issuer = $ca->getIssuer(); 
    }

	public function testAddUsr() {
		Database::addUsr($this->uid, $this->cert, $this->issuer);
	}


	public function testGetcert() {
		Database::addUsr($this->uid, $this->cert, $this->issuer);
		$this->assertEquals(Database::getcert($this->uid), $this->cert);
	}

	/**
     * @expectedException Exception
     */
	public function testDoubleEntryOfUid(){
		Database::addUsr($this->uid, $this->cert, $this->issuer);
		Database::addUsr($this->uid, $this->cert, $this->issuer);
	}

	/**
     * @expectedException Exception
     */
	public function testGetNonExistingValue(){
		$x = Database::getCert($this->uid);
	}


	public function testAddEncryptionKey(){

		Database::addUsr($this->uid, $this->cert, $this->issuer);
		$res = Database::addEncryptionKey($this->uid,$this->pCryptKey,$this->signedPCryptKey);
	}


	public function testGetPublicEncryptionKey(){

		Database::addUsr($this->uid, $this->cert, $this->issuer);

		Database::addEncryptionKey($this->uid,$this->pCryptKey,$this->signedPCryptKey);
		$res = Database::getPublicEncryptionKey($this->uid);
		$this->assertTrue($res && count($res) == 4);
		$this->assertTrue($res['cert'] != null);
		$this->assertTrue($res['issuer'] != null);
		$this->assertTrue($res['cryptkey'] != null);
		$this->assertTrue($res['cryptkeysign'] != null);
	}

	public function testSetRevocationHash(){
		Database::addUsr($this->uid, $this->cert, $this->issuer);
		Database::setRevocationHash($this->uid, PwdRevocation::buildSaltedHash("password"));
	}

	/**
     * @expectedException Exception
     */
	public function testSetRevocationHashTwice(){
		Database::addUsr($this->uid, $this->cert, $this->issuer);
		Database::setRevocationHash($this->uid, PwdRevocation::buildSaltedHash("password"));
		Database::setRevocationHash($this->uid, PwdRevocation::buildSaltedHash("asdqwe"));
	}

	public function testGetRevocationHash(){
		Database::addUsr($this->uid, $this->cert, $this->issuer);
		$hash = PwdRevocation::buildSaltedHash("password");
		Database::setRevocationHash($this->uid, $hash);
		$this->assertEquals($hash, Database::getRevocationHash($this->uid));
	}

	/**
     * @expectedException Exception
     */
	public function testRevokeKeys(){
		Database::addUsr($this->uid, $this->cert, $this->issuer);
		Database::addEncryptionKey($this->uid,$this->pCryptKey,$this->signedPCryptKey);
		$arr = Database::getPublicEncryptionKey($this->uid);
		$this->assertEquals($arr['cryptkey'], $this->pCryptKey);
		Database::revokeKeys($this->uid);
		Database::getPublicEncryptionKey($this->uid);
	}

	

}