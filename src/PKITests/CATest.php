<?php

namespace Safest\PKI\PKItests;

use Safest\PKI\CA;
use Safest\PKI\Config;
//require_once 'PHPUnit/Autoload.php';

//require 'vendor/PingPal/PPSecServer/Config.php';

class CATest extends \PHPUnit_Framework_TestCase{

	private $ca;
    private $csr_str;
    private $signed_cert;


	protected function setUp()
    {
        $this->ca = new CA();
    
        $config = array(
    		//"digest_alg" => "sha512",
    		"private_key_bits" => 2048,
    		"private_key_type" => OPENSSL_KEYTYPE_RSA,
		);
	
        $dn = array(
    	"countryName" => "UK",
    	"stateOrProvinceName" => "Somerset",
    	"localityName" => "Glastonbury",
    	"organizationName" => "The Brain Room Limited",
    	"organizationalUnitName" => "PHP Documentation Team",
    	"commonName" => "123456789012345678901234567890123",
    	"emailAddress" => "wez@example.com"
		);

		$privkey = openssl_pkey_new($config);
		
		$csr = openssl_csr_new($dn, $privkey);
		openssl_csr_export($csr, $this->csr_str);

		$this->signed_cert = $this->ca->signCSR($this->csr_str);
    }

	public function testSignCSR(){

		$this->expectOutputRegex("/-----BEGIN CERTIFICATE-----/");
		print $this->ca->signCSR($this->csr_str);
	
	}

	public function testExtractUidCSR(){

		$this->expectOutputRegex('/^[a-zA-Z0-9]{33}$/');

		print $this->ca->extractUidCSR($this->csr_str);

	}

	public function testExtractUidCert(){

		$this->expectOutputRegex('/^[a-zA-Z0-9]{33}$/');
		print $this->ca->extractUidCert($this->signed_cert);

	}


	public function testVerifySignature(){
		$pCryptKey = "Bf5CTECtxkXZSBA799ES5fw1maKtjRlUtADOpTwMHE4=";
    	$uid = "oaekxuasxgmcrjytaartlhglygqqhayv";
    	$signedPCryptKey = "NJvFVG5SXHR9PRejNskP3Cp3T4u8dL4YuLPfUyJO50H0tRS7zcTCHftDcC1m6vKU\nWhgB1tVB1aRAh0eDWGhyzdxR5A9ymmlsrlw7hhOV8Os/1cIWALAzrXd+NYV4utP9\nJpXLY2z2KACKfYK5UR6Jg/SSbRVp2lbBgR4RW3CJqQeCXoYhw+S9kwhXGCrrYdd0\ngUUFWpAAbZ9sF85PqUnv+X/+IdyyZ5NQTf1S0u5imWVGLlzTgibYVpC88Az8rK1U\nDlsXfx/F2DBq0azRtZLY2dcWOcjQSeD5KDK0CpdFWk/XuvBfVGxKiuYOYDqGKuRx\noM4E0SXTRJn2Jx2F3y1/Dg==";
		$cert = "-----BEGIN CERTIFICATE-----\nMIIEKDCCAxCgAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjDELMAkGA1UEBhMCU0Ux\nDzANBgNVBAgTBlNjYW5pYTENMAsGA1UEBxMETHVuZDETMBEGA1UEChMKUGluZ1Bh\nbCBBQjEMMAoGA1UECxMDRGV2MRgwFgYDVQQDEw9QaW5nUGFsIG1haW4gQ0ExIDAe\nBgkqhkiG9w0BCQEWEWpvYWtpbUBwaW5ncGFsLnNlMB4XDTE0MDYxMDA5MzU0NVoX\nDTE1MDYxMDA5MzU0NVowKzEpMCcGA1UEAwwgb2Fla3h1YXN4Z21jcmp5dGFhcnRs\naGdseWdxcWhheXYwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDec4T0\nb1FP3vdwnbU4zbPiuWUNcnsEio/Cc40kWusDDEGubBLWqlu+0jobeqG2nw/rkP2E\nCmilEANPul3Uk3URyfP0pJLNmid9X46JMiziIvzbwkrbrstt5TkMN2URJG0HR+Vp\n5IuqiZaySeLdM+7m8dSy1YKNn7b2fSM42fDkBFGPzAQWtNfiL2YTkVA8tzJ97394\n8DHXXT/wivUBKGTVLnjLwX+v944Q3OAoEgdLuHWkR85LayB8h42SLOMb+S06YdiX\n3IAx5+xqAiQRGKomJ54kR+KvrHibxNMJMOAX82hwUI5IdubNf65Qml+EHyJjIyNH\nf5DPQiY3ZtvIGwPhAgMBAAGjgfQwgfEwHQYDVR0OBBYEFPe63UhSlRiQrZR9ZkZ0\nihZYw8glMIHBBgNVHSMEgbkwgbaAFDPBZQ7aqlkCLk29KiB0R+oVnwzPoYGSpIGP\nMIGMMQswCQYDVQQGEwJTRTEPMA0GA1UECBMGU2NhbmlhMQ0wCwYDVQQHEwRMdW5k\nMRMwEQYDVQQKEwpQaW5nUGFsIEFCMQwwCgYDVQQLEwNEZXYxGDAWBgNVBAMTD1Bp\nbmdQYWwgbWFpbiBDQTEgMB4GCSqGSIb3DQEJARYRam9ha2ltQHBpbmdwYWwuc2WC\nCQCemMuJ2IeabDAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQA+t3It\nm7bTFLVQQK+d8xnAQ7S7lM7dOKoLAMaoQsGFwZ6Fm/5sfYHxsvNAubEfExsywmuj\nLtsf3KOQrtGnufSJ+Ap0QrutxdfzTV5vF3n9TpAf10HQBfFRcB+OiyckuZF9JdrX\nC6wdegxaEmkv2clsWbjRR9LgiTVOdyQffSt5WO/VtzMI3eadd2jHt9Bgk246zjJp\ngrD4qMEMcHLd3jP0jJg+dUvr9AZvpIWDIwerNRPMkPeQUq5QhNTYnWn8C1WykidR\nN0+yVcsAncnIYDpTJFcarnLX54WAdaHlBqf495+LGUYdmh0iJK/LECvgwgkTbRhc\nJmXuxoxXpPjV267T\n-----END CERTIFICATE-----";
		$test = $this->ca->verifySignature($cert,$signedPCryptKey,$pCryptKey);
		$this->assertTrue($test);
	}

	public function testGetIssuer(){
		$cacertpath = 'file://'.realpath(Config::$CA_CERT_PATH);
		openssl_x509_export($cacertpath, $cacert);
		$this->assertEquals($this->ca->getIssuer(), $cacert);
	}
}

?>