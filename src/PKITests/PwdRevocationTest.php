<?php

namespace Safest\PKI\PKItests;

use Safest\PKI\PwdRevocation;
use Safest\PKI\CA;
use Safest\PKI\Database;

class PwdRevocationTest extends \PHPUnit_Framework_TestCase{


	protected function setUp(){
       	$this->uid = substr(md5(rand()), 0, 32) . "s";
       	$this->pCryptKey = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 44);
		$this->signedPCryptKey = "NJvFVG5SXHR9PRejNskP3Cp3T4u8dL4YuLPfUyJO50H0tRS7zcTCHftDcC1m6vKU\nWhgB1tVB1aRAh0eDWGhyzdxR5A9ymmlsrlw7hhOV8Os/1cIWALAzrXd+NYV4utP9\nJpXLY2z2KACKfYK5UR6Jg/SSbRVp2lbBgR4RW3CJqQeCXoYhw+S9kwhXGCrrYdd0\ngUUFWpAAbZ9sF85PqUnv+X/+IdyyZ5NQTf1S0u5imWVGLlzTgibYVpC88Az8rK1U\nDlsXfx/F2DBq0azRtZLY2dcWOcjQSeD5KDK0CpdFWk/XuvBfVGxKiuYOYDqGKuRx\noM4E0SXTRJn2Jx2F3y1/Dg==";
    	$ca = new CA();
    	$this->issuer = $ca->getIssuer(); 
    }

	public function testBuildSaltedHash(){

		$this->expectOutputRegex("~^\\$[a-zA-Z0-9]+\\$\d+\\$\S+$~");
		print PwdRevocation::buildSaltedHash(md5(rand()));
	
	}

	public function testVerifyPass(){
		$pass = md5(rand());
		$falseHash = '$2y$12$QjSH496pcT5CEbzjD/vtVeH03tfHKFy36d4J0Ltp3lRtee9HDxY3K';
		$hash = PwdRevocation::buildSaltedHash($pass);
		$this->assertTrue(PwdRevocation::verifyPass($pass, $hash));
		$this->assertFalse(PwdRevocation::verifyPass($pass, $falseHash));
	}


	public function testSetPass(){

		Database::addUsr($this->uid, "test", $this->issuer);

		PwdRevocation::setPass("password", $this->uid);
	}

	/**
     * @expectedException Exception
     */
	public function testRevokeKeysWrongPassword(){
		Database::addUsr($this->uid, "test");

		PwdRevocation::setPass("password", $this->uid);
		PwdRevocation::revokeKeys("ilikecats", $this->uid);
	}

	/**
     * @expectedException Exception
     */
	public function testRevokeKeys(){
		Database::addUsr($this->uid, "test");
		PwdRevocation::setPass("password", $this->uid);

		Database::addEncryptionKey($this->uid,$this->pCryptKey,$this->signedPCryptKey);
		$arr = Database::getPublicEncryptionKey($this->uid);
		$this->assertEquals($arr['cryptkey'], $this->pCryptKey);

		PwdRevocation::revokeKeys("password", $this->uid);
		$arr = Database::getPublicEncryptionKey($this->uid);
	}

}

?>