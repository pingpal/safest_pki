<?php

namespace Safest\PKI\PKITests;

use Safest\PKI\CertRevocationDB;

class CertRevocationDBTest extends \PHPUnit_FrameWork_TestCase{

	public function testAddRevokedCert() {
		$rand1 = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1).substr(md5(time()),1);
		$rand2 = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1).substr(md5(time()),1);
		
		CertRevocationDB::addRevokedCert($rand1, $rand2);

	}

	public function testGetRevokedCerts() {
		$rand1 = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1).substr(md5(time()),1);
		$rand2 = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1).substr(md5(time()),1);
		
		CertRevocationDB::addRevokedCert($rand1, $rand2);

		$res = CertRevocationDB::getRevokedCerts();

		$worked = false;
		for ($i=0; $i < count($res); $i++) { 
			$row = $res[$i];
			if ($row['cert'] === $rand1) {
				$worked = true;
			}
		}
		$this->assertTrue($worked);
	}

}