<?php

namespace Safest\PKI;
use Exception;

class PwdRevocation {

	static function buildSaltedHash($pass){
		return password_hash($pass, PASSWORD_DEFAULT);
	}

	static function verifyPass($pass, $hash){
		return password_verify($pass, $hash);
	}

	static function setPass($pass, $uid){
		
		if (strlen($pass) < 6) {
			
			throw new Exception("Chosen password too short, minimum length is 6 characters");
			
		}

		$hash = PwdRevocation::buildSaltedHash($pass);
		Database::setRevocationHash($uid, $hash);
	}

	static function revokeKeys($pass, $uid){
		$db_hash = Database::getRevocationHash($uid);

		if (PwdRevocation::verifyPass($pass, $db_hash)){
			Database::revokeKeys($uid);
		} else {
			throw new Exception("Wrong password");			
		}
	}

}