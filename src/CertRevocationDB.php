<?php

namespace Safest\PKI;

/**
 * # alias rev
 *
 * # table rev
 * # engine InnoDB
 *
 * # column id 				id
 * # column cert 			varchar(4096)
 * # column revoketime		
 */

class CertRevocationDB {
	
	static function addRevokedCert($cert, $time){
		$sql = sql()->insert()->into('rev');
		
		$sql->set('cert', $cert);
		$sql->set('revoketime', $time);
		$sql->exe();
	}

	static function getRevokedCerts(){
		$sql = sql()->select()->from('rev');
		return $sql->all();
	}


}