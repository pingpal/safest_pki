nginx=$1 #path to nginx dir
debug=$2
conf=$3

rm -rf certs
mkdir certs
cd certs
openssl genrsa -des3 -out root_ca.key 4096
openssl req -new -x509 -days 3650 -key root_ca.key -out root_ca.crt

openssl genrsa -des3 -out pki.key 2048
openssl req -new -key pki.key -out pki.csr
openssl x509 -req -days 3650 -in pki.csr -CA root_ca.crt -CAkey root_ca.key -set_serial 01 -out pki.crt

openssl genrsa -des3 -out pp.key 2048
openssl req -new -key pp.key -out pp.csr
openssl x509 -req -days 3650 -in pp.csr -CA root_ca.crt -CAkey root_ca.key -set_serial 02 -out pp.crt

cat pp.crt root_ca.crt > pp_ca_chain.crt
cat pki.crt root_ca.crt > pki_ca_chain.crt

if [ $debug -eq 1 ]
	then
	openssl genrsa -des3 -out dev.key 2048
	openssl req -new -key dev.key -out dev.csr
	openssl x509 -req -days 3650 -in dev.csr -CA pp.crt -CAkey pp.key -set_serial 03 -out dev.crt
fi

cd ..

rm -rf $nginx/certs
mkdir $nginx/certs
cp certs/* $nginx/certs
cp $conf $nginx/nginx.conf