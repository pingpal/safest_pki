<?php

namespace Safest\PKI;

use Illusion\Config as IllConfig;

class Config {
	static $CERT_VAL_PERIOD = 3650;
	static $CA_KEY_PASSPHRASE = "";
	static $CA_KEY_PATH = "/vendor/safest/pki/src/Resources/certs/pki.key";
	static $CA_CERT_PATH = "/vendor/safest/pki/src/Resources/certs/pki.crt";
	static $OPENSSL_CONFIG_PATH = "/vendor/safest/pki/src/Resources/openssl.cnf";

	static $KEY_CSR = "csr";
	static $KEY_CERT = "cert";
	static $KEY_UID = "uid";
	static $KEY_PCRYPT = "cryptkey";
	static $KEY_PCRYPTSIGN = "cryptkeysign";
	static $KEY_PASS = "pass";

	static $USR_PIN = "upin";
	static $USR_PIN_SIGN = "upinsign";

}

Config::$CA_KEY_PATH = IllConfig::$DOCROOT . Config::$CA_KEY_PATH;
Config::$CA_CERT_PATH = IllConfig::$DOCROOT . Config::$CA_CERT_PATH;
Config::$OPENSSL_CONFIG_PATH = IllConfig::$DOCROOT . Config::$OPENSSL_CONFIG_PATH;

Config::$CA_KEY_PASSPHRASE = include 'keypassphrase.php';
