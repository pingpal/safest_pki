<?php

namespace Safest\PKI;

use Illusion\Core\Session;

use Illusion\Core\Util;

use Illusion\Config as IllConfig;

use Exception;
/**
 * # alias api.pki
 * # route post:/addusr			api	api.pki.addusr
 * # route post:/setusrpin		api	api.pki.setusrpin
 * # route post:/addenc			api	api.pki.addenc
 * # route post:/getcrypt 		api	api.pki.getcrypt
 * # route post:/revokekeys		api	api.pki.revokekeys
 * # route post:/setrevokepass	api	api.pki.setrevokepass
 * # route post:/getrevoked		api	api.pki.gerevoked
 * # route post:/test			api	api.pki.test
 */

class API {

	/**
	 * # access	public
	 * # alias hay
	 */

	static function hay($args, $in, $out) {

		$out->end(__DIR__);
	}

	/**
	 * # access	public
	 * # alias setusrpin
	 */

	static function _setusrpin($args, $in, $out) {
		
		try {

			$uid = @$args[Config::$KEY_UID];
			$uid = API::getDevID() . $uid;
			$pin = @$args[Config::$USR_PIN];
			$pinsign = @$args[Config::$USR_PIN_SIGN]; //sign(pin + sign)

			if (!$uid || !$pin || !$pinsign){
				throw new Exception( "all required fields not found");
			}

			API::setusrpin($uid, $pin, $pinsign);

		} catch (Exception $e) {
			$out->setStatusCode(400);
			throw $e;
		}
}

	/**
	 * # access	public
	 * # alias getrevoked
	 */

	static function _getRevoked($args, $in, $out) {
		
		try {

			$out->end(API::getRevoked());

		} catch (Exception $e) {

			$out->setStatusCode(400);
			throw $e;
		}
}

	/**
	 * # access	public
	 * # alias revokekeys
	 */

	static function _revokeKeys($args, $in, $out) {

		try {

			API::revokeKeys(@$args[Config::$KEY_UID], @$args[Config::$KEY_PASS]);

		} catch (Exception $e) {

			$out->setStatusCode(400);
			throw $e;
		}

	}

	/**
	 * # access	public
	 * # alias setrevokepass
	 */

	static function _setRevokePass($args, $in, $out) {

		try {

			API::setRevokePass(@$args[Config::$KEY_UID], @$args[Config::$KEY_PASS]);

		} catch (Exception $e) {

			$out->setStatusCode(400);
			throw $e;
		}

	}

	/**
	 * # access	public
	 * # alias addusr
	 */

	static function _addUsr($args, $in, $out) {

		try {
			$csr = @$args[Config::$KEY_CSR];
			$pin = @$args[Config::$USR_PIN];

			$out->end(API::addUsr($csr, $pin));

		} catch (Exception $e) {

			$out->setStatusCode(400);
			throw $e;
		}
	}

	/**
	 * # access	public
	 * # alias addenc
	 */
	static function _addEncryptionKey($args, $in, $out){

		try {

			$uid = @$args[Config::$KEY_UID];
			$pCrypt = @$args[Config::$KEY_PCRYPT];
			$pCryptSign = @$args[Config::$KEY_PCRYPTSIGN];

			//var_dump("key:" . $pCrypt);
			//var_dump("sign:" . $pCryptSign);

			API::addEncryptionKey($uid, $pCrypt, $pCryptSign);			

		} catch (Exception $e) {

			$out->setStatusCode(400);
			throw $e;
		}

	}

	/**
	 * # access	public
	 * # alias getcrypt
	 */
	static function _getPublicEncryptionKey($args, $in, $out){

		try {

			$uid = @$args[Config::$KEY_UID];
			
			$out->end(API::getPublicEncryptionKey($uid));

		} catch (Exception $e) {

			$out->setStatusCode(400);
			throw $e;
		}
	}

	static function setusrpin($uid, $pin, $pinsign){
		//TODO kolla om pinsign är valid
		Database::setPin($uid, $pin);	
	}

	static function getRevoked(){
		return CertRevocationDB::getRevokedCerts();
	}

	static function addUsr($csr, $pin){


		$csr = API::parseCsr($csr);
	    $ca = new CA();
	    $signed_cert = $ca->signCSR($csr);

	    //var_dump($signed_cert);

	    $signed_cert = API::parseCert($signed_cert);

	    $uid = $ca->extractUidCert($signed_cert);
	    $uid = API::getDevID() . $uid;
	    $issuer = $ca->getIssuer();

	    $dbpin = Database::getPin($uid);

		if((strcmp($pin, $dbpin)) != 0){
			throw new Exception("incorrect pin");
		}	
		//TODO remove pin

		Database::addUsr($uid, $signed_cert, $issuer);
		//echo("hay");
		return $signed_cert;
	}

	static function addEncryptionKey($uid, $pCrypt, $pCryptSign){

		$uid = API::getDevID() . $uid;
		$ca = new CA();
		$cert = Database::getCert($uid);

		if ($cert && $ca->verifySignature($cert,$pCryptSign,$pCrypt)) {

	    	Database::addEncryptionKey($uid,$pCrypt,$pCryptSign);
	    
	    } else {

	    	throw new Exception("signature not valid");
	    
	    }
	}

	static function getPublicEncryptionKey($uid){

		$uid = API::getDevID() . $uid;
		return Database::getPublicEncryptionKey($uid);

	}

	static function revokeKeys($uid, $pass) {
		$uid = API::getDevID() . $uid;
		PwdRevocation::revokeKeys($pass, $uid);
	}

	static function setRevokePass($uid, $pass) {
		$uid = API::getDevID() . $uid;
		PwdRevocation::setPass($pass, $uid);
	}

	static function parseCsr($csr) {
		$csr = str_replace("NEW ", "", $csr);

		$begin = "-----BEGIN CERTIFICATE REQUEST-----";
		$end = "-----END CERTIFICATE REQUEST-----";

		preg_match("/^\s*$begin\s+((.|\n|\r)*)\s+$end\s*$/", $csr, $match);

		$x = ($match && count($match)) ? $match[1] : $csr;

		$csr = str_replace(" ", "\n", $x);
		$csr = "$begin\n$csr\n$end";
		$csr = preg_replace("/\r?\n|\r/", "\n", $csr);

		return $csr;
	}

	static function parseCert($cert) {
		$cert = str_replace("NEW ", "", $cert);

		$begin = "-----BEGIN CERTIFICATE-----";
		$end = "-----END CERTIFICATE-----";

		preg_match("/^\s*$begin\s+((.|\n|\r)*)\s+$end\s*$/", $cert, $match);

		$x = ($match && count($match)) ? $match[1] : $cert;

		$cert = str_replace(" ", "\n", $x);
		$cert = "$begin\n$cert\n$end";
		$cert = preg_replace("/\r?\n|\r/", "\n", $cert);

		return $cert;
	}

	static function getDevID(){
		return "safestdev"; // TODO
		$devID = $_SERVER['DN'];
		preg_match("~/CN=((?:.|\s)+)(?:/|\\\|$)~", $devID, $match);
		if (! @$match[1]) {
			throw new Exception("Error extracting developer ID", 1);
			
		}
		return $match[1];
	}
}
