<?php

namespace Safest\PKI;

use Illusion\Core\Session;

use Illusion\Core\Util;

use Exception;

/**
 * # alias pki
 *
 * # table pki
 * # engine InnoDB
 *
 * # column id 				id
 * # column uid 
 * # column cert 			varchar(4096)
 * # column issuer 			varchar(4096)
 * # column authkey 		varchar(2048)
 * # column cryptkey 		varchar(45)
 * # column cryptkeysign 	varchar(512)
 *
 * # column revokehash		varchar(256)
 * # column revoked 		boolean
 *
 * # column pin 			varchar(16) 
 *
 * # custom unique(uid)
 */

class Database {

	static function access() {

		return true;
	}

	static function setPin($uid, $pin){

		
		$sql = sql()->insert()->into('pki');
		$sql->set('pin', $pin);
		$sql->set('uid', $uid);
		$sql->exe();
	}
	
	static function getPin($uid){

		$sql = sql()->select()->from('pki');
		$sql->where('uid', $uid);
		$sql->field('pin');
		$ret = $sql->one();
		if(! $ret) throw new Exception("No entry for given uid", 1);
		
		return $ret;
	}

	static function addUsr($uid, $cert, $issuer) {

		$sql = sql()->insert()->into('pki');
		
		//$sql->set('uid', $uid);
		$sql = sql()->update()->table('pki')->where('uid', $uid);
		$sql->set('cert', $cert);
		$sql->set('issuer', $issuer);
		$sql->exe();
	}

	static function addEncryptionKey($uid, $publicKey, $signature){

		if (!is_string($publicKey) || strlen($publicKey) != 44){
			throw new Exception("Bad public key." . strlen($publicKey));
		}

		$sql = sql()->update()->table('pki')->where('uid', $uid);
		
		$sql->set('cryptkey', $publicKey);
		$sql->set('cryptkeysign', $signature);
		$sql->exe();

	}

	static function getCert($uid){

		$sql = sql()->select()->from('pki');
		$sql->where('uid', $uid);
		$sql->field('cert');
		$ret = $sql->one();
		if(! $ret) throw new Exception("No entry for given uid", 1);
		
		return $ret;
	}

	static function getPublicEncryptionKey($uid){
		

		$sql = sql()->select()->from('pki');
		$sql->where('uid', $uid);
		$sql->field('revoked');
		$rev = $sql->one();

		if ($rev){
			throw new Exception("This user has revoked the encryption key");
		}

		$sql = sql()->select()->from('pki');
		$sql->where('uid', $uid);
		$sql->field('cert');
		$sql->field('issuer');
		$sql->field('cryptkey');
		$sql->field('cryptkeysign');
		$ret = $sql->row();
		if(! $ret) throw new Exception("No entry for given uid", 1);

		return $ret;
	}

	static function setRevocationHash($uid, $hash){


		$sql = sql()->select()->from('pki');
		$sql->where('uid', $uid);
		$sql->field('revokehash');
		if ($sql->one()) {
			throw new Exception("Updating revocation password is forbidden");
		}

		$sql = sql()->update()->table('pki');
		
		$sql->where('uid', $uid);
		$sql->set('revokehash', $hash);
		$affectedRows = $sql->exe();		

		if(! $affectedRows)throw new Exception("No such user");
		
	}

	static function getRevocationHash($uid){


		$sql = sql()->select()->from('pki');
		$sql->where('uid', $uid);
		$sql->field('revokehash');
		return $sql->one();
	}

	static function revokeKeys($uid){


		$sql = sql()->update()->table('pki');
		
		$sql->where('uid', $uid);
		$sql->set('revoked', 1);
		$sql->set('cryptkey', "");
		$sql->set('cryptkeysign', "");
		$sql->set('authkey', "");
		$sql->set('cert', "");
		$sql->exe();
	}	

	static function selectHelper($uid, $field){
		$sql = sql()->select()->from('pki');
		$sql->where('uid', $uid);
		$sql->field($field);
		return $sql->one();
	}
}